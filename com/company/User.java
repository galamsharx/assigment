package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class User {
    private static int id_gen;
    File file = new File("db.txt");
    Scanner fileScanner = new Scanner(file);
    Scanner stringScanner;
    private int id;
    private String name;
    private String surname;
    private String username;
    private Password password;
    private String role;

    public User(String name, String surname, String username, Password password, String role) throws FileNotFoundException {
        while (fileScanner.hasNext()) {
            String string = fileScanner.nextLine();
            stringScanner = new Scanner(string);
        }
        if (stringScanner != null) {
            id_gen = stringScanner.nextInt();
        }
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    public User(int id, String name, String surname, String username, Password password, String role) throws FileNotFoundException {
        this.id = id;
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    protected void generateId() {
        id_gen++;
        this.id = id_gen;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public String toString() {
        return "ID : " + getId() + " | " + "Username : " + getUsername() + "\n" + "Name : " + getName() + " | " + "Surname : "
                + getSurname() + " | " + "Role : " + getRole();
    }

    public int compareTo(User compareUser) {
        int compareId = compareUser.getId();
        return this.id - compareId;
    }
}
